# -*- coding: utf-8 -*-
from threading import Thread
import re
import urllib.request
from bs4 import BeautifulSoup
from collections import Counter
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from datetime import datetime
from flask import Flask, request, jsonify
import json
from operator import itemgetter


# import matplotlib.pyplot as plt
# from matplotlib.image import imread
SLACK_TOKEN = ?
SLACK_SIGNING_SECRET = ?

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


def _crawl_keywords(text):
    if ("네이버" in text):
        result = _crawl_naver_keywords(text)
    elif ('다음' in text):
        result = _crawl_daum_keywords(text)
    elif ("music" in text or "음악" in text or "노래" in text):
        result = _crawl_music_chart(text)
    elif ("뉴스" in text or "news" in text or "기사" in text):
        result = _news_(text)
    elif ("날씨" in text or "공기" in text or "대기" in text):
        result = _crawl_forecast(text)
    elif ("운세" in text or "자리" in text or "띠" in text):
        result = _crawl_fortune(text)
    elif ("몇시" in text or "시간" in text):
        result = _crawl_time_(text)
    else:
        result = "Today"

    return result


def _ssafy_food_(text):
    my_block = ImageBlock(
        image_url="https://edu.ssafy.com/data/upload_files/namo/images/000004/20190708082304576_18S6XNQP.png",
        alt_text="기다려봐방")
    return my_block
    # img=plt.imread("food7_2.bmp")
    # plt.imshow(img)
    # plt.show()


def _crawl_fortune(text):
    if "자리" not in text and "띠" not in text:
        return "오류"
    # 여기에 함수를 구현해봅시다.
    key = text.split(' ')[1]
    query_text = urllib.parse.quote_plus(key + " 운세")
    query_text = query_text.replace("%5C", "%")
    url = 'https://search.naver.com/search.naver?where=nexearch&sm=tab_etc&query=' + query_text
    tag_name = 'div'
    class_name = 'contents03'
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    # 여기에 함수를 구현해봅시다.
    keywords = []
    for tag in soup.find_all(tag_name, class_=class_name):
        tag2 = tag.find('p', class_='text _cs_fortune_text')
        word1 = tag2.get_text().strip()
        keywords.append(':crystal_ball:*' + key + ' 오늘의 운세*:crystal_ball:\n\n' + word1)
    message = '\n'.join(keywords)
    return message


def _crawl_forecast(text):
    keywords = []
    tmp = text.split(' ')
    txt = ' '.join(tmp[1:])  # 지역이 들어감
    query_text = urllib.parse.quote_plus(txt + "+날씨")
    query_text = query_text.replace("%5C", "%")
    search_url = "https://search.naver.com/search.naver?sm=tab_hty.top&where=nexearch&query=" + query_text

    source_code = urllib.request.urlopen(search_url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    # 여기에 함수를 구현해봅시다.

    # loc = soup.find('span', class_='btn_select').get_text() #지역
    keywords.append(':sunny:*오늘의 날씨*:sunny:\n')
    keywords.append(txt + "입니다^^")
    tag = soup.find('div', class_='info_data')  # 온도
    temp = tag.get_text().strip()[:3]
    cast = tag.find('p', class_='cast_txt').get_text()  # 흐림
    keywords.append(temp + '℃ \n' + cast)

    tag_name = "dl"
    class_name = "indicator"

    for a_tag in soup.find_all(tag_name, class_=class_name):
        myL = a_tag.get_text().strip()
        keywords.append(myL)

    message = '\n'.join(keywords)
    return message


def _news_(text):
    keywords = []
    keywords.append("♨*오늘의 헤드라인 뉴스입니다.*♨\n")
    tmp = text.split(' ')
    txt = tmp[1]  # 지역이 들어감
    query_text = urllib.parse.quote_plus(txt + "+날씨")
    query_text = query_text.replace("%5C", "%")
    search_url = "https://news.naver.com/main/home.nhn"

    tag_name = "div"
    class_name = "hdline_article_tit"
    source_code = urllib.request.urlopen(search_url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    for a_tag in soup.find_all(tag_name, class_=class_name):
        link = a_tag.find("a")["href"].strip()
        link = "https://news.naver.com" + link
        myL = a_tag.get_text().strip()
        # print("<" + link + ">")
        # print(myL)
        if myL: keywords.append(myL + "\n" + "더 자세히 보시려면...." + link)
    # 키워드 리스트를 문자열로 만듭니다.

    return '\n'.join(keywords)


# 크롤링 음악
def _crawl_music_chart(text):
    message = []
    # 여기에 함수를 구현해봅시다.\
    message.append("*♬벅스 실시간 차트입니다♬*\n")
    url = "https://music.bugs.co.kr/"
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    ######################################################
    HERE = soup.find("div", class_="trackChart")
    rank = []
    for i in range(1, 11):
        rank.append(str(i) + "위 : ")
    title = []
    for tag in HERE.find_all("p", class_="title"):
        title.append(tag.get_text().strip() + "/")
    artist = []
    for tag in HERE.find_all("p", class_="artist"):
        tmp = tag.get_text().strip().split('\n')
        artist.append(tmp[0])

    for i in range(10):
        tmp = ""
        tmp = rank[i] + title[i] + artist[i]
        message.append(tmp)

    return '\n'.join(message)


# 크롤링 함수 구현하기
def _crawl_naver_keywords(text):
    # 여기에 함수를 구현해봅시다.
    keywords = []
    keywords.append(":memo:*네이버 실시간 검색어 1위부터 20위까지 입니다.*:memo:")
    url = "http://www.naver.com"
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    rank = 1
    for span_tag in soup.find_all("span", class_="ah_k"):
        myL = span_tag.get_text().strip()
        keywords.append(str(rank) + "위: " + myL)
        rank += 1
    # 키워드 리스트를 문자열로 만듭니다.

    return '\n'.join(keywords[:21])


def _crawl_daum_keywords(text):
    # 여기에 함수를 구현해봅시다.
    keywords = []
    keywords.append(":memo:*다음 실시간 검색어 1위부터 10위까지 입니다.*:memo:")
    url = "http://www.daum.net"
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    rank = []
    for i in range(10):
        rank.append(str(i + 1) + "위: ")
    for span_tag in soup.find_all("span", class_="txt_issue"):
        myL = span_tag.get_text().strip()
        if not myL in keywords:
            keywords.append(myL)
            # 키워드 리스트를 문자열로 만듭니다.
    message = []
    for i in range(10):
        message.append(rank[i] + keywords[i + 1])

    return keywords[0] + '\n' + '\n'.join(message[:10])


def ment():
    block1 = SectionBlock(
        text="*안녕하세요, Today 챗봇입니다.*\n\n*알고 싶으신 키워드를 붙여 멘션해주세요!*\n\n`ex`\n\n"
    )
    block2 = SectionBlock(
        fields=["*<실시간 검색어>*\n`@Today 네이버`  `@Today 다음`\n\n", "*<실시간 음악 차트>*\n`@Today music`  `@Today 노래`\n\n"]
    )
    block3 = SectionBlock(
        fields=["*<매일 업데이트되는 뉴스>*\n`@Today 뉴스`  `@Today news`\n\n", "*<날씨 및 대기 상황>*\n`@Today 날씨`  `@Today 공기`  `@Today 대기`\n\n"]
    )
    block4 = SectionBlock(
        text="*<띠별 & 별자리별 운세>*\n`@Today 쥐띠 운세`   `@Today 사수자리 운세`"
    )
    block5 = SectionBlock(
        text="*<오늘 싸피 식단이 궁금해?>*\n`@Today 오늘 밥뭐야?`"
    )
    block6 = SectionBlock(
        text ="*<지금 초 단위까지 알고싶어>*\n`@Today 지금 몇시야`"
    )
    return [block1, block2, block3, block4,block5,block6]


def _crawl_time_(text):
    t = ['월', '화', '수', '목', '금', '토', '일']

    keywords = []
    now = datetime.now()
    keywords.append("현재 시간은")
    keywords.append(str(now)[:19])
    keywords.append("입니다.\n")
    week = datetime.today().weekday()
    keywords.append("오늘은 " + t[week] + "요일 이네요~")
    return ' '.join(keywords)


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    slack_web_client.chat_postMessage(channel=channel, blocks=extract_json(ment()))
    # _process(text, channel)
    Thread(target=_process, args=(text,channel)).start()

def _process(text,channel):
    if "밥" in text or "식단" in text or "싸피" in text :
        ssafy = []
        ssafy.append(extract_json(_ssafy_food_(text)))
        slack_web_client.chat_postMessage(channel=channel, blocks=ssafy)

    else:
        keywords = _crawl_keywords(text)
        slack_web_client.chat_postMessage(channel=channel, text=keywords)


# 챗봇이 1:1 DM을 받았을 때
@slack_events_adaptor.on("message")
def on_message_received(event_data):
    pass


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=8080)
